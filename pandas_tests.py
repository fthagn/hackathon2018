import pandas as pd
df = pd.read_csv('TRUMPF_TruLaserCenter_Dataset_2018.csv')
df_test = pd.DataFrame({'A': ['a', 'b', 'a'], 'B': ['b', 'a', 'c'], 'C': [1, 2, 3]})

df_test_result = pd.get_dummies(df_test)

df_result = pd.get_dummies(df,
                           prefix=['id_03006_Z_pushout_modus',
                                   'id_03063_B_N_E1',
                                   'id_00002_name_lst',
                                   'id_00003_name_teil',
                                   'id_00004_maschinennummer',
                                   'id_00006_framework_version',
                                   'id_01004_cut_tech']
                           )
print("Success")