#%%

# sudo pip3 install catboost, datei darf nicht catboost.py heissen

import numpy as np
import pickle
from catboost import CatBoostClassifier, Pool, cv
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
import pandas as pd
import random
from sklearn import datasets
from sklearn.cross_validation import train_test_split
#%%

df = pd.read_csv('TRUMPF_TruLaserCenter_Dataset_2018.csv')
df.fillna(-999, inplace=True)

#%%
indizes = df['id_00002_name_lst'].unique()

#%%
def process(filename):
    result = df[df['id_00002_name_lst'] == filename]
    return result


cm_list = []
cr_list = []
acc_list = []
importances_list = []
spec_list = []

for i in range(5):

    print("TRAINING: {}".format(i))

    split_df = [process(fn) for fn in indizes]
    random.shuffle(split_df)

    train_list = []
    validation_list = []
    test_list = []
    rows_counter = 0
    train_split_rate = 0.7
    valid_split_rate = 0.15
    test_split_set = 0.15

    for dataframe in split_df:
        if rows_counter < (train_split_rate * df.shape[0]):
            train_list.append(dataframe)
        elif rows_counter < ((train_split_rate + valid_split_rate) * df.shape[0]):
            validation_list.append(dataframe)
        else:
            test_list.append(dataframe)
        rows_counter = rows_counter + dataframe.shape[0]


    train_df = pd.concat(train_list)
    valid_df = pd.concat(validation_list)
    test_df = pd.concat(test_list)


    X_train = train_df.drop(['id_13008_RGT_erfolgreich_1try', 'DataID', 'id_00002_name_lst', 'id_00004_maschinennummer', 'id_00006_framework_version', 'id_01001_density', 'id_13001_date', 'id_00003_name_teil'], axis=1)
    y_train = train_df['id_13008_RGT_erfolgreich_1try']
    X_valid = valid_df.drop(['id_13008_RGT_erfolgreich_1try', 'DataID', 'id_00002_name_lst', 'id_00004_maschinennummer', 'id_00006_framework_version', 'id_01001_density', 'id_13001_date', 'id_00003_name_teil'], axis=1)
    y_valid = valid_df['id_13008_RGT_erfolgreich_1try']
    X_test = test_df.drop(['id_13008_RGT_erfolgreich_1try', 'DataID', 'id_00002_name_lst', 'id_00004_maschinennummer', 'id_00006_framework_version', 'id_01001_density', 'id_13001_date', 'id_00003_name_teil'], axis=1)
    y_test = test_df['id_13008_RGT_erfolgreich_1try']


    # print(X_train.dtypes)

    categorical_features_indices = np.where(X_train.dtypes != np.float)[0]


    model = CatBoostClassifier(
        custom_loss=['Accuracy'],
        logging_level='Silent',
        task_type = "GPU",
        use_best_model=True,
        od_type='Iter',
        od_wait=60,
        l2_leaf_reg=10,
        iterations=10000,
        learning_rate=0.18,
        random_strength=5,
        class_weights=[5, 1]
    )


    model.fit(
        X_train, y_train,
        cat_features=categorical_features_indices,
        eval_set=(X_valid, y_valid),
        logging_level='Verbose',  # you can uncomment this for text output
        plot=True
    )



    y_pred = model.predict(X_test)

    cm = confusion_matrix(y_test, y_pred)
    target_names = ['class 0', 'class 1']
    cr = classification_report(y_test, y_pred, target_names=target_names)
    #print(cm.ravel())
    tn, fp, fn, tp = cm.ravel()
    specificity = tn / (tn+fp)
    print('Simple model tree count: {}'.format(model.tree_count_))
    print(cm)
    print(cr)
    print("Specificity: {}".format(specificity))
    acc = accuracy_score(y_test, y_pred)
    print(acc)


    importances = model.get_feature_importance(data=None,
                                               prettified=True,
                                               thread_count=-1,
                                               verbose=True)

    cm_list.append(cm)
    cr_list.append(cr)
    acc_list.append(acc)
    spec_list.append(specificity)
    importances_list.append(importances)

cm_accumulated = np.sum(np.array(cm_list), axis=0)
np.savetxt('results/weights_confusion_matrix_test.txt', cm_accumulated)

acc_accumulated = np.array(acc_list)
np.savetxt('results/weights_mean_accuracy.txt', acc_accumulated)

spec_accumulated = np.array(spec_list)
np.savetxt('results/weights_mean_specificity.txt', spec_accumulated)

import json
with open('results/weights_importances_list.json', 'w') as outfile:
    json.dump(importances_list, outfile)